# %%
import sys
sys.path.append('../../')
from supplychainmodelhelper import graphoperations as go

import numpy as np
from supplychainmodelhelper import graphoperations as go
import pandas as pd

# creating the graph
prod = ['milk', 'beer', 'schnaps', 'vodka']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL', 'P']
myNW = go.createGraph(listOfActors=act,listOfLocations=loc,listOfProducts=prod)

# Creating Distance matrix
myData={loc[0]:[1,2,5,10],loc[1]:[2,1,5,8],loc[2]:[5,5,1,12],loc[3]:[10,8,12,1]}
myDF = pd.DataFrame(myData,index=loc)

# supply and demand
sendingProd = [[10,50,40,100],[11,45,44,50],[15,55,30,20],[100,50,20,120]]
receivingProd = [[30,30,40,100],[40,30,30,50],[10,70,20,20],[80,0,90,120]]

# chosen number of transport distances
soManyTDs = 10
myTDs = []
allFlows = np.ndarray((len(loc),len(loc),len(prod),soManyTDs))

# creating list of node IDs of participants
for run,p in enumerate(prod):
    print('\n product: '+p)
    senderIDs = go.getListOfNodeIDs(
        myNW,
        actors=['producer'],
        products=[p]
    )
    receiverIDs = go.getListOfNodeIDs(
        myNW,
        actors=['consumer'],
        products=[p]
    )

    # creating edgeIDs for adding 'distance' to graph
    allcombIDs1, allcombIDs2 = go.getAllCombinations(
        senderIDs,
        receiverIDs,
        order='1st'
    )
    myEdges4Graph = go.getEdgeID(myNW, allcombIDs1, allcombIDs2)
    didItWork3 = go.addAttr2Edges(
        myNW,
        myEdges4Graph,
        myDF.values.flatten(),
        attr='distance'
    )

    go.addAttr2ExistingNodes(myNW,senderIDs,'output',sendingProd[run])
    go.addAttr2ExistingNodes(myNW,receiverIDs,'input',receivingProd[run])

    # get the transport distances
    myTDs.append(
        go.calcPotTransportDistances(
            myNW,
            listOfSenderIDs=senderIDs,
            listOfReceiverIDs=receiverIDs,
            nrOfValues=soManyTDs
        )
    )

    # Run the gravity model with given transport distance and return the flow
    # each myTDs[run] is a list of 5 transport distances
    # allFlows is input for the calibration algorithm
    '''for it,td in enumerate(myTDs[run]):
        print('\n td: '+str(td)+', it: '+str(it))
        allFlows[0:len(senderIDs),0:len(receiverIDs),run,it] = \
            go.hymanModel(
                myNW, 
                listOfSenderIDs=senderIDs, 
                listOfReceiverIDs=receiverIDs, 
                transportDistance=td, 
                tolerance = 0.01
                )
'''
#creating calibration measure that halfway might make sense
'''allFoodTransported = sum(sum(sendingProd,[]))
dummy = np.random.rand(
    len(loc)**2-1
)*allFoodTransported/80+allFoodTransported/(len(sum(sendingProd,[]))+1)
myCalMat = np.append(
    dummy,allFoodTransported-np.sum(dummy)
).reshape((len(senderIDs),len(receiverIDs)))

print(go.calibration(allFlows,myCalMat))
'''# %%

# %%
