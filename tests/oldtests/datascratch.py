# %%

import sys

sys.path.append('../../')
from supplychainmodelhelper import mds

myDC = mds.Datacube('myscratchtest.hdf5','new')
myFolderList2FillOut = ['newfolder', '1', 'This is a test', \
    'new kind of description text to describe the reason for the existence of this folder']
myDC.addFolder2ExistingDB(listOfEntries=myFolderList2FillOut)

myFolderList2FillOut = ['anewtest', '2', 'This is a new test', \
    'some kind of description text to describe the reason for the living of this folder']
myDC.addFolder2ExistingDB(listOfEntries=myFolderList2FillOut)

myDC.importMDDataofDatasetFromCSV('newfolder','../../templates/myTemplate.csv')

print(myDC.getMDFromDB())
#print(myDC.getMDFromFolder('newfolder'))
#print(myDC.getMDFromDataSet('newfolder','dasD1'))
# %%
