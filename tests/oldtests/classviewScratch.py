import networkx as nx

from supplychainmodelhelper import graphoperations as go
import pandas as pd
from supplychainmodelhelper.drawer import Drawer

# creating the graph
prod = ['milk']
act = ['producer', 'store', 'consumer']
loc = ['BER', 'SXF', 'TXL', 'P']
myNW = go.createGraph(listOfActors=act,
                      listOfLocations=loc,
                      listOfProducts=prod)

# Creating Distance matrix
myDistance = {loc[0]: [1, 2, 5, 10],
              loc[1]: [2, 1, 5, 8],
              loc[2]: [5, 5, 1,12],
              loc[3]: [10, 8, 12, 1]}
myDF = pd.DataFrame(myDistance, index=loc)

# entering distance information to the right nodes on the graph
p = 'milk'
senderIDs = go.getListOfNodeIDs(
    myNW,
    actors=['producer'],
    products=[p])
receiverIDs = go.getListOfNodeIDs(
    myNW,
    actors=['consumer'],
    products=[p])

# supply and demand
sendingProd = [10, 50, 40, 1000]
receivingProd = [30, 30, 40, 100]

go.addAttr2ExistingNodes(myNW, senderIDs, 'output', sendingProd)
go.addAttr2ExistingNodes(myNW, receiverIDs, 'input', receivingProd)

#print(myNW.nodes(data=True))

allcombIDs1, allcombIDs2 = go.getAllCombinations(
    senderIDs,
    receiverIDs,
    order='1st')
myEdges4Graph = go.getEdgeID(
    myNW,
    allcombIDs1,
    allcombIDs2)
go.addAttr2Edges(
    myNW,
    myEdges4Graph,
    myDF.values.flatten(),
    attr='distance')
go.addAttr2Edges(
    myNW,
    myEdges4Graph,
    myDF.values.flatten(),
    attr='weight')
#print(go.getExistingAttrs(SCGraph=myNW, gtype='n'))


###########################################################################
d = Drawer(myNW)
#d.node_size()
#pos = nx.multipartite_layout(myNW, subset_key='output')
#nx.draw(myNW,pos)
d.draw_it()