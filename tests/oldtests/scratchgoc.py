#%%
from supplychainmodelhelper import graphoperationsCLASS as go

prod = ['milk','beer','schnaps']
act = ['producer','consumer','warehouse','store']
loc = ['BER','SXF','TXL']

myG = go.SCgraph(actors=act,locations=loc,products=prod)
# %%
