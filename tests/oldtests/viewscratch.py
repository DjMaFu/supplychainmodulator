switch = 2

if switch == 0:

    from networkx.algorithms import bipartite

    from supplychainmodelhelper import graphoperations as go
    import pandas as pd

    # creating the graph
    prod = ['milk']
    act = ['producer','consumer']
    loc = ['BER','SXF','TXL','P']
    myNW = go.createGraph(listOfActors=act,listOfLocations=loc,listOfProducts=prod)

    # Creating Distance matrix
    myDistance={loc[0]:[1,2,5,10],loc[1]:[2,1,5,8],loc[2]:[5,5,1,12],loc[3]:[10,8,12,1]}
    myDF = pd.DataFrame(myDistance,index=loc)


    # entering distance information to the right nodes on the graph
    p = 'milk'
    senderIDs = go.getListOfNodeIDs(
        myNW,
        actors=['producer'],
        products=[p])
    receiverIDs = go.getListOfNodeIDs(
        myNW,
        actors=['consumer'],
        products=[p])
    allcombIDs1, allcombIDs2 = go.getAllCombinations(
        senderIDs,
        receiverIDs,
        order='1st')
    myEdges4Graph = go.getEdgeID(
        myNW,
        allcombIDs1,
        allcombIDs2)
    go.addAttr2Edges(
        myNW,
        myEdges4Graph,
        myDF.values.flatten(),
        attr='distance')

    # drawing
    import networkx as nx
    #nx.draw_networkx(
    #    myNW,
     #   pos = nx.drawing.layout.bipartite_layout(
      #      myNW,
        #    senderIDs),
       # width = 5) # Or whatever other display options you like
    #nx.draw(myNW)

    x = set(senderIDs)
    #print(x)
    y = set(receiverIDs)
    #print(y)

    colors = range(20)
    pos = dict()
    pos.update( (n, (i, 2)) for i, n in enumerate(x) ) # put nodes from X at x=1
    pos.update( (n, (i, 1)) for i, n in enumerate(y) ) # put nodes from Y at x=2
    # TODO : create stages for different actors
    # TODO: create colors for different products
    # TODO : create edge width for amount transported
    # TODO: maybe distance color coding for edges
    import matplotlib.pyplot as plt
    nx.draw_networkx(
        myNW,
        pos=pos,
        edge_color=colors,
        edge_cmap=plt.cm.hot)

    plt.show()
elif switch == 1:
    import matplotlib.pyplot as plt
    import networkx as nx

    G = nx.star_graph(20)
    pos = nx.spring_layout(G, seed=63)  # Seed layout for reproducibility
    colors = range(20)
    options = {
        "node_color": "#A0CBE2",
        "edge_color": colors,
        "width": 4,
        "edge_cmap": plt.cm.viridis,
        "with_labels": False,
    }
    nx.draw(G, pos, **options)
    plt.show()
    ##########################################################################
    ##########################################################################
    ##########################################################################
    ##########################################################################
    ##########################################################################
    ##########################################################################
    ##########################################################################
    ##########################################################################
elif switch==2:
    from supplychainmodelhelper import graphoperations as go
    import pandas as pd

    # creating the graph
    prod = ['milk']
    act = ['producer', 'consumer']
    loc = ['BER', 'SXF', 'TXL', 'P']
    myNW = go.createGraph(listOfActors=act, listOfLocations=loc, listOfProducts=prod)

    # Creating Distance matrix
    myDistance = {loc[0]: [1, 2, 5, 10], loc[1]: [2, 1, 5, 8], loc[2]: [5, 5, 1, 12], loc[3]: [10, 8, 12, 1]}
    myDF = pd.DataFrame(myDistance, index=loc)

    # entering distance information to the right nodes on the graph
    p = 'milk'
    senderIDs = go.getListOfNodeIDs(
        myNW,
        actors=['producer'],
        products=[p])
    receiverIDs = go.getListOfNodeIDs(
        myNW,
        actors=['consumer'],
        products=[p])

    # supply and demand
    sendingProd = [10, 50, 40, 10000]
    receivingProd = [30, 30, 40, 100]

    go.addAttr2ExistingNodes(myNW, senderIDs, 'output', sendingProd)
    go.addAttr2ExistingNodes(myNW, receiverIDs, 'input', receivingProd)

    print(myNW.nodes(data=True))

    allcombIDs1, allcombIDs2 = go.getAllCombinations(
        senderIDs,
        receiverIDs,
        order='1st')
    myEdges4Graph = go.getEdgeID(
        myNW,
        allcombIDs1,
        allcombIDs2)
    go.addAttr2Edges(
        myNW,
        myEdges4Graph,
        myDF.values.flatten(),
        attr='distance')
    go.addAttr2Edges(
        myNW,
        myEdges4Graph,
        myDF.values.flatten(),
        attr='myattr')
    print(go.getExistingAttrs(SCGraph=myNW, gtype='n'))
    ##########################################################################
    ##########################################################################
    ##########################################################################
    # drawing
    import networkx as nx
    import matplotlib.pyplot as plt
    x = set(senderIDs)
    y = set(receiverIDs)

    pos = dict()
    pos.update((n, (i, 2)) for i, n in enumerate(x))  # put nodes from X at x=1
    pos.update((n, (i, 1)) for i, n in enumerate(y))  # put nodes from Y at x=2
    colors = range(20)

    edges, weights = zip(*nx.get_edge_attributes(myNW, 'distance').items())

    nodes_out, myout = zip(*nx.get_node_attributes(myNW, 'output').items())
    nodes_in, myin = zip(*nx.get_node_attributes(myNW, 'input').items())
    #print(myout)

    min_dist = min(weights)
    max_dist = max(weights)
    #print(min(weights))

    options = {
        "edgelist": edges,
        "edge_color": weights,
        "width": 4,
        "edge_cmap": plt.cm.coolwarm,
        "with_labels": False,
    }
    fig, ax = plt.subplots()
    nx.draw(myNW, pos, **options, ax=ax)
    ax.tick_params(left=True, bottom=False, labelleft=True, labelbottom=False)
    ax.set_yticks([2,1])
    ax.set_yticklabels(act)

    #nx.draw(myNW, ax=ax)
    mymap = plt.cm.coolwarm
    min_dist = min(weights)
    max_dist = max(weights)

    sm = plt.cm.ScalarMappable(cmap=mymap, norm=plt.Normalize(vmin=min_dist, vmax=max_dist))
    sm._A = []
    #plt.colorbar(sm)

    """nodes_out = nx.draw_networkx_nodes(myNW,
                                   pos,
                                   node_color='k',
                                   nodelist=nodes_out,
                                   node_size=myout)
    nodes_in = nx.draw_networkx_nodes(myNW,
                                    pos,
                                    node_color='g',
                                    nodelist=nodes_in,
                                    node_size=myin)

    edgesD = nx.draw_networkx_edges(myNW,
                                   pos,
                                   edge_color=weights,
                                   edgelist=edges,
                                   width=4,
                                   edge_cmap=mymap)
"""
    # find the edge with min/max <<edge_label>>
    #min_edge = min(myNW.edges(data=True), key=lambda x: x[2]['distance'])
    #max_edge = max(myNW.edges(data=True), key=lambda x: x[2]['distance'])
    #max_edge = max(dict(myNW.edges).items(), key=lambda x: x[1]['distance'])
    #_, all_vals = zip(*dict(myNW.edges).items())
    #all_vals.values()
    #max_vals = max(all_vals)

    #print(min_vals)
    #print(t)
    #print(all_vals.values())

    #plt.colorbar(edgesD)
    #plt.figure(dpi=100)
    #plt.grid('on')
    plt.axis('on')
    plt.show()


    from itertools import chain
    print("all nodes")
    print(myNW.nodes(data=True))
    print("have")
    print([list(x.values()) for x in dict(myNW.nodes(data=True)).values()])
    print("detail")
    print([x.values() for x in dict(myNW.nodes(data=True)).values()])
    print([x for x in dict(myNW.nodes(data=True)).values()])
    print([x for x in dict(myNW.nodes(data='actor')).values()])
    print("want")
    print(list(set(chain(*[list(x.values()) \
                           for x in dict(myNW.nodes(data=True)).values()]))))
    print("huh")
    print(set([x['actor'] for x in dict(myNW.nodes(data=True)).values()]))
    #print([list(x.values()) for x in dict(myNW.nodes(data='actor')).values()])