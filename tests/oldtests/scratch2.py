# %%

''' if you need an explanation on how to use a particular function, please have a look at 
help(function)

example:
help(go.combineActorBrand)
or 
help(mds.Datacube)

'''
import sys

sys.path.append('../../')
from supplychainmodelhelper import graphoperations as go
import numpy as np

X = np.random.randint(0,10,40_000_000).reshape((400,400,50,5))
calTest = np.random.randint(0,10,160_000).reshape((400,400))
myTest = go.calibration(X,calTest)
print(myTest)
print('done')


#
#
# # %%
# import sys
# sys.path.append('../../')
#
# import numpy as np
# from supplychainmodelhelper import graphoperations as go
# import pandas as pd
#
# # creating the graph
# prod = ['milk','beer','schnaps']
# act = ['producer','consumer','warehouse','store']
# loc = ['BER','SXF','TXL']
# myNW = go.createGraph(listOfActors=act,listOfLocations=loc,listOfProducts=prod)
#
# # Creating Distance matrix
# myData={loc[0]:[1,2,50],loc[1]:[2,1,49],loc[2]:[50,49,1]}
# myDF = pd.DataFrame(myData,index=loc)
#
# # supply and demand
# sendingProd = [[10,50,40],[11,45,44],[15,55,30]]
# receivingProd = [[30,30,40],[40,30,30],[10,70,20]]
#
# myTDs = []
# allFlows = np.ndarray((3,3,3,5))
#
# # creating list of node IDs of participants
# for run,p in enumerate(prod):
#     # creating edgeIDs for adding 'distance' to graph
#     senderIDs = go.getListOfNodeIDs(myNW, actors=['producer'],products=[p])
#     receiverIDs = go.getListOfNodeIDs(myNW, actors=['consumer'],products=[p])
#
#     allcombIDs1,allcombIDs2 = go.getAllCombinations(senderIDs,receiverIDs,order='1st')
#     myEdges4Graph = go.getEdgeID(myNW,allcombIDs1,allcombIDs2)
#     didItWork3 = go.addAttr2Edges(myNW,myEdges4Graph, myDF.values.flatten(),attr='distance')
#
#
#     go.addAttr2ExistingNodes(myNW,senderIDs,'output',sendingProd[run])
#     go.addAttr2ExistingNodes(myNW,receiverIDs,'input',receivingProd[run])
#
#     # get the transport distances
#     myTDs.append(go.calcPotTransportDistances(myNW, listOfSenderIDs=senderIDs, listOfReceiverIDs=receiverIDs))
#
#     # Run the gravity model with given transport distance and return the flow
#     for it,td in enumerate(myTDs[run]):
#         allFlows[0:len(senderIDs),0:len(receiverIDs),run,it] = \
#             go.hymanModel(myNW, listOfSenderIDs=senderIDs, listOfReceiverIDs=receiverIDs, \
#                 transportDistance=td, tolerance = 0.01)
#
# # %%

# %%
