"""
purpose: get edge id from corresponding list of nodes to be connected
Note this is a one-to-one connection: the first element of outgoingNodes is
ONLY connected to the first element in incomingNodes. If you want to return
a list of edgeIDs with all possible combinations, either create 2 lists of
all possible combinations yourself or make use of
myNewList1,myNewList2 = getAllCombinations(list1,list2)
NOTE this function doesnt check if any of the edges already contain
information


:param SCGraph: the graph, needed for confirmation that the nodes to be
connected do indeed exist
:param outgoingNodes: a list of NodeIDs of existing nodes in SCGraph
(can be retrieved with getNodeID)
:param incomingNodes: a list of NodeIDs  of existing nodes in SCGraph
(can be retrieved with getNodeID)

:return: a list of tuples (outgoingNodes,incomingNodes)

example:
from supplychainmodelhelper import graphoperations as go

prod = ['milk','beer','schnaps']
act = ['producer','consumer','warehouse','store']
loc = ['BER','SXF','TXL']

myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)
producerIDs = go.getListOfNodeIDs(SCGraph=myNW, actors=['producer'])
consumerOfSchnapsIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    products=['schnaps'],
    actors=['producer']
)
allcomb1,allcomb2 = go.getAllCombinations(
    producerIDs,
    consumerOfSchnapsIDs
)

myEdgeIDs = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=allcomb1,
    incomingNodes=allcomb2
)

===========================================================================
"""
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']

myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)
producerIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer']
)
consumerOfSchnapsIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    products=['schnaps'],
    actors=['producer']
)
allcomb1, allcomb2 = go.getAllCombinations(
    list1=producerIDs,
    list2=consumerOfSchnapsIDs
)

myEdgeIDs = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=allcomb1,
    incomingNodes=allcomb2
)

print(myEdgeIDs)

