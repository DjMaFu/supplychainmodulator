"""
purpose: If all edges between listOfSenderIDs and listOfReceiverIDs
contain the attribute edgeattrDistName, this function returns a numpy
matrix of all distances between all combinations of participating nodes.
Needed for hymanModel, but user may need it for other purposes.
Technically, this can be used as a getEdgeAttribute function.

:param SCGraph: the graph with information about outgoing goods from
senders and incoming goods for receivers
:param listOfSenderIDs: list of node IDs in G (should have 'output'
attribute)
:param listOfReceiverIDs: list of node IDs in G (should have 'input'
attribute)
:param edgeattrDistName: str name of the edge attribute for the
geographical distance between nodes listOfSenderIDs, order of rows should
be identical to listOfReceiverIDs,
NOTE hymanModel expects this to be 'distance' !

:return: a numpy matrix of all combinations of distances between
participating nodes

example:




===========================================================================
"""
from supplychainmodelhelper import graphoperations as go
import pandas as pd

# creating the graph
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

# Creating Distance matrix
myData = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
myDF = pd.DataFrame(myData, index=loc)

# creating list of node IDs of participants
senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)
receiverIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer'],
    products=['milk']
)

# creating edgeIDs for adding 'distance' to graph
allcombIDs1, allcombIDs2 = go.getAllCombinations(
    list1=senderIDs,
    list2=receiverIDs,
    order='1st'
)
myEdges4Graph = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=allcombIDs1,
    incomingNodes=allcombIDs2
)
didItWork3 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdges4Graph,
    listOfContent=myDF.values.flatten(),
    attr='distance'
)

print(
    go.getDistMatrix(
        SCGraph=myNW,
        listOfSenderIDs=senderIDs,
        listOfReceiverIDs=receiverIDs,
        edgeattrDistName='distance'
    )
)
