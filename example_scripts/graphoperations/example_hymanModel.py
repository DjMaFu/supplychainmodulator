"""
purpose: Gravity Model. calculate the flow of goods, based on a transport
distance (got from calcPotTransportDistances), by estimating unkown beta.
Hardcoded here is the exponential distance model, input for furnessModel
(see furnessModel for details).
If the user wants to create their own distance model, the user may start
with furness, probably wants to create their own version of hyman, as well.

1. initial guess of beta0 = 1./transportDistance
2. calculate flow based on beta_0: flow_0=furnessModel(beta_0)
3. calculate currentTransportDistance_0 =
    getWeightedDist(flow_0,getDistMatrix)
4. start iterative process
    4a. calculate beta_i(beta_i-1, currentTransportDistance_0)
    4b. calculate flow_i=furnessModel(beta_i)
    4c. calculate currentTransportDistance_i =
        getWeightedDist(flow_i,getDistMatrix)
    4d. check if abs(currentTransportDistance_i - transportDistance)
        < tolerance -> break cycle
5. return flow=furnessModel(beta_i)

:param SCGraph: the graph with information about outgoing goods from
senders and incoming goods for receivers
:param listOfSenderIDs: list of node IDs in G (should have 'output'
attribute)
:param listOfReceiverIDs: list of node IDs in G (should have 'input'
attribute)
:param transportDistance: weighted average transport distance. Calculated
via calcPotTransportDistances
:param tolerance: (optional) parameter of how good the model should fit
the given input data. default is 0.01 of the unit 'weight' of the edges
of the graph

:return: flow between supplier and receiver nodes based on given transport
distance. Needed for calibration vs. total flow of all goods

example:



===========================================================================
"""
from supplychainmodelhelper import graphoperations as go
import pandas as pd

# creating the graph
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

# creating list of node IDs of participants
senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)
receiverIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer'],
    products=['milk']
)

# Creating Distance matrix
myData = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
myDF = pd.DataFrame(myData, index=loc)

# creating edgeIDs for adding 'distance' to graph
allcombIDs1, allcombIDs2 = go.getAllCombinations(
    list1=senderIDs,
    list2=receiverIDs,
    order='1st'
)
myEdges4Graph = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=allcombIDs1,
    incomingNodes=allcombIDs2
)
didItWork3 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdges4Graph,
    listOfContent=myDF.values.flatten(),
    attr='distance'
)

# supply and demand
sendingMilk = [10, 50, 40]
receivingMilk = [30, 30, 40]
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=senderIDs,
    nameOfAttr='output',
    listOfAttr=sendingMilk
)
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=receiverIDs,
    nameOfAttr='input',
    listOfAttr=receivingMilk
)

# get the transport distances
myTDs = go.calcPotTransportDistances(
    SCGraph=myNW,
    listOfSenderIDs=senderIDs,
    listOfReceiverIDs=receiverIDs
)

# Run the gravity model with given transport distance and return the flow
print(
    go.hymanModel(
        SCGraph=myNW,
        listOfSenderIDs=senderIDs,
        listOfReceiverIDs=receiverIDs,
        transportDistance=myTDs[0],
        tolerance=0.01
    )
)
