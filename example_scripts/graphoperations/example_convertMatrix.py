"""
purpose: converts the input matrix to a contracted version or a widened
version of itself
m = dim(haveSenderAttrs), n = dim(haveReceiverAttrs)
dim(inputMatrix) = mXn
p = dim(wantSenderAttrs), q = dim(wantReceiverAttrs)
An mXn matrix is converted into a pXq matrix.
If p<m and q<n, the contracted rows and columns are added to each other,
respectively: Application for calibrationProcess.

If p>m and q>n, the new rows and columns are copied their contents,
respectively: Application for quick creation of a distance matrix with
different actors, sending or receiving.

input:
:param haveSenderAttrs: the list of senders connected to the flow matrix
:param haveReceiverAttrs: the list of receivers connected to the flow
matrix
:param inputMatrix: the matrix to either be contracted or extended.
:param wantSenderAttrs: a list of the attributes of the senders
:param wantReceiverAttrs: a list of the attributes of the receivers

:return the converted matrix

example (for both application types, respectively):


===========================================================================
"""

from supplychainmodelhelper import graphoperations as go
import pandas as pd

# creating the graph
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
        listOfActors=act,
        listOfLocations=loc,
        listOfProducts=prod
)

# creating list of node IDs of participants
ids_producer_milk = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)
ids_warehouse_milk = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['warehouse'],
    products=['milk']
)
ids_consumer_milk = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer'],
    products=['milk']
)
ids_producer_beer = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['beer']
)
ids_consumer_beer = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer'],
    products=['beer']
)



# Creating Distance matrix
myData = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
myDF = pd.DataFrame(myData, index=loc)


# get the content 'location' of the participating nodes
_, location_producer_milk = go.convertLoT2NTs(
    go.getNodeIDswAttr(
        SCGraph=myNW,
        nameOfAttr='location',
        listOfNodeIDs=ids_producer_milk
    )
)
_, location_consumer_milk = go.convertLoT2NTs(
    go.getNodeIDswAttr(
        SCGraph=myNW,
        nameOfAttr='location',
        listOfNodeIDs=ids_consumer_milk
    )
)
_, location_producer_beer = go.convertLoT2NTs(
    go.getNodeIDswAttr(
        SCGraph=myNW,
        nameOfAttr='location',
        listOfNodeIDs=ids_producer_beer
    )
)
_, location_consumer_beer = go.convertLoT2NTs(
    go.getNodeIDswAttr(
        SCGraph=myNW,
        nameOfAttr='location',
        listOfNodeIDs=ids_consumer_beer
    )
)

# EXAMPLE 1: expansion
myTrafo = go.convertMatrix(
    haveSenderAttrs=loc,
    haveReceiverAttrs=loc,
    inputMatrix=myDF.to_numpy(),
    wantSenderAttrs=location_producer_milk+location_producer_beer,
    wantReceiverAttrs=location_consumer_milk+location_consumer_beer+ids_warehouse_milk
)

# creating edgeIDs for adding 'distance' to graph
# needed, because any edge involved in the gravity model needs a distance
# property
allcombIDs1, allcombIDs2 = go.getAllCombinations(
    list1=ids_producer_milk+ids_producer_beer,
    list2=ids_consumer_milk+ids_consumer_beer+ids_warehouse_milk,
    order='1st'
)
myEdges4Graph = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=allcombIDs1,
    incomingNodes=allcombIDs2
)
# adding the distance information to all involved edges simultaneously
didItWork3 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdges4Graph,
    listOfContent=myTrafo.flatten().tolist(),
    attr='distance'
)

###############################################################################
# EXAMPLE 2 (contraction)
# creating list of node IDs of participants
senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)
receiverIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer', 'warehouse'],
    products=['milk']
)

# supply and demand
sendingMilk = [10, 50, 40]
receivingMilk = [10, 15, 20, 15, 15, 25]
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=senderIDs,
    nameOfAttr='output',
    listOfAttr=sendingMilk
)
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=receiverIDs,
    nameOfAttr='input',
    listOfAttr=receivingMilk
)

# get the transport distances
myTDs = go.calcPotTransportDistances(
    SCGraph=myNW,
    listOfSenderIDs=senderIDs,
    listOfReceiverIDs=receiverIDs
)

# Run the gravity model with given transport distance and return the flow
flow = go.hymanModel(
    SCGraph=myNW,
    listOfSenderIDs=senderIDs,
    listOfReceiverIDs=receiverIDs,
    transportDistance=myTDs[0],
    tolerance=0.01
)


locFlowOnly = go.convertMatrix(
    haveSenderAttrs=senderIDs,
    haveReceiverAttrs=receiverIDs,
    inputMatrix=flow,
    wantSenderAttrs=loc,
    wantReceiverAttrs=loc
)

