"""
purpose: get me the edge attributes content from the edges i provide.

:param SCGraph: the graph in question.
:param attr(optional): An existing attribute of an edge in the graph.
default:'weight'. Learn about existing attributes in the graph via
the function "getExistingAttrs".
:param listOfEdgeIDs(optional): A list of edge IDs given. default:
all active edges (gotten from "getEdgeID").

:return: A list of attribute content in the same order as the edge
IDs given.

example:


===========================================================================
"""
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

producerIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer']
)
consumerOfSchnapsIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    products=['schnaps'],
    actors=['producer']
)
myEdgeIDs = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=producerIDs,
    incomingNodes=consumerOfSchnapsIDs
)

listOfShipping = [10, 1, 2000]
listOfDistances = [50.2, 10.3, 111.2]

didItWork1 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdgeIDs,
    listOfContent=listOfShipping
)
didItWork2 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdgeIDs,
    listOfContent=listOfDistances,
    attr='distance'
)

# all active edges with attribute 'weight'
print(go.getEdgesAttr(SCGraph=myNW))


print("active edges with attribute 'distance'")
for eid in myEdgeIDs:
    print(
        go.getEdgesAttr(
            SCGraph=myNW,
            attr='distance',
            listOfEdgeIDs=[eid]
        )
    ) # just one edge

