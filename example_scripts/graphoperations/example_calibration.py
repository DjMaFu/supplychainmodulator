"""
purpose: calibrate the calculated flow against a calibration measure
calculate the flow of goods, based on a transport distance
(got from calcPotTransportDistances), by estimating the free and
unknown parameter beta (see furnessModel).
Hardcoded here is the exponential distance model, input for furnessModel
(see furnessModel for details).
The hymanModel allows to calculate the flow if the average transport
distance is known. Unfortunately only a range of possible average
transport distances for each product are known (and may be calculated by
the function calcPotTransportDistances).
In Germany a Federal Transport Infrastructure Plan(FTIP) exists, that gives
information about how much food is transported in average from
one county to another.
For this to be calibrated against the flow of all products involved needs
to summed up. calcPotTransportDistances returns for every product a number
of possible average transport distances (m).

Manual for input: create a 4D-numpy array for the input parameter
flowTensor

>> import numpy as np
>> np.ndarray((
    rowsOfFlow,
    colsOfFlow,
    nrOfProducts,
    nrOfTransportationDistances
))

with
- rowsOfFlow, colsOfFlow defined by the dimensions of your flow matrix
- nrOfProducts as the number of products in your supply chain
- nrOfTransportationDistances as the number of transport distances from
calcPotTransportDistances

The other is the calibration measure (expected is of dimension
[rowsOfFlow,colsOfFlow]).
If you have a calibration measure differing from your flows, consider using
convertMatrix from this toolkit to convert your flow to the dimension of
the parameter calibrationMatrix.

The formula involved in this algorithm to find the best possible
transport distance (m):
Min(abs(sum_products abs(flow_products_[m] - FTIP)))


:param flowTensor: 4D tensor numpy.ndarray [i,j,p,m] = [output, input,
product, member of the ensemble]
:param calibrationMatrix: calibration measure of dimension [output, input]

:return listOfBestEnsembleConfiguration: a list of ids that correspond to
the transport distances m. For each product a specific m was chosen,
so that the difference between the input flows and the calibration
measure in minimized.
May be accessed via the command
>> flowTensor[0:output,0:input,0:products,listOfBestEnsembleConfiguration]


===========================================================================
"""
import numpy as np
from supplychainmodelhelper import graphoperations as go
import pandas as pd

# creating the actors, locations and products
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']


###############################################################################
# TOY DATA
###############################################################################
# Creating Distance matrix (presumably in km)
myData = {
    loc[0]: [1, 5, 10],
    loc[1]: [5, 1, 9],
    loc[2]: [10, 9, 1]
}

# supply (presumably in t)
sendingProd = [
    [14, 33, 33],
    [53, 34, 13],
    [33, 33, 34]
]
# demand (presumably in t)
receivingProd = [
    [34, 13, 33],
    [33, 34, 33],
    [33, 33, 34]
]
# aggregated food transport matrix from 1 location to the other
# (presumably in t)
# PLEASE NOTE: the following data set should reflect the model, such that
# intra-location shipping should be higher than inter-location shipping,
# otherwise the algorithm(gravity model) produces errors, trying to fix this
# paradox
ftip_toy_data = np.array(
    [
        [50.0, 20.0, 10.0],
        [20.0, 50.0, 30.0],
        [10.0, 20.0, 70.0]
    ]
)
###############################################################################



# actually creating the graph
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

myDF = pd.DataFrame(
    myData,
    index=loc
)

myTDs = []
# dim1: output
# dim2: input
# dim3: product
# dim4: average transport distance slot nr
allFlows = np.ndarray((3, 3, 3, 5))

# creating list of node IDs of participants
for run, p in enumerate(prod):
    senderIDs = go.getListOfNodeIDs(
        SCGraph=myNW,
        actors=['producer'],
        products=[p]
    )
    receiverIDs = go.getListOfNodeIDs(
        SCGraph=myNW,
        actors=['consumer'],
        products=[p]
    )

    # creating edgeIDs for adding 'distance' to graph
    allcombIDs1, allcombIDs2 = go.getAllCombinations(
        list1=senderIDs,
        list2=receiverIDs,
        order='1st'
    )
    myEdges4Graph = go.getEdgeID(myNW, allcombIDs1, allcombIDs2)
    didItWork3 = go.addAttr2Edges(
        SCGraph=myNW,
        listOfEdgeIDs=myEdges4Graph,
        listOfContent=myDF.values.flatten(),
        attr='distance'
    )

    go.addAttr2ExistingNodes(
        SCGraph=myNW,
        listOfNodeIDs=senderIDs,
        nameOfAttr='output',
        listOfAttr=sendingProd[run]
    )
    go.addAttr2ExistingNodes(
        SCGraph=myNW,
        listOfNodeIDs=receiverIDs,
        nameOfAttr='input',
        listOfAttr=receivingProd[run]
    )

    # get the transport distances
    myTDs.append(
        go.calcPotTransportDistances(
            SCGraph=myNW,
            listOfSenderIDs=senderIDs,
            listOfReceiverIDs=receiverIDs
        )
    )

    # Run the gravity model with given transport distance and return the
    # flow each myTDs[run] is a list of 5 transport distances
    # allFlows is input for the calibration algorithm
    for it, td in enumerate(myTDs[run]):
        allFlows[
            0:len(senderIDs),
            0:len(receiverIDs),
            run,
            it
        ] = go.hymanModel(
            SCGraph=myNW,
            listOfSenderIDs=senderIDs,
            listOfReceiverIDs=receiverIDs,
            transportDistance=td,
            tolerance=0.01
        )

# creating calibration measure that halfway might make sense
allFoodTransported = sum(sum(sendingProd, []))
my_ensembles = go.calibration(allFlows, ftip_toy_data)

# quick and dirty calculation of the correct flows with the calibrated
# transport distances
for i, p in enumerate(prod):
    print(
        "\ntransport distance for " +
        str(p) +
        ": " +
        str(myTDs[i][my_ensembles[i]])
    )
    print("flows for " + str(p) + ":")
    print(allFlows[:, :, i,  my_ensembles[i]])

# quick and dirty summing up
print("\nsumming up allFlows output (should match output data):")
print(
    np.sum(
        allFlows[:, :, 0, my_ensembles[0]] +
        allFlows[:, :, 1, my_ensembles[1]] +
        allFlows[:, :, 2, my_ensembles[2]],
        1
    )
)

print("\nsumming up allFlows input (should match input data):")
print(
    np.sum(
        allFlows[:, :, 0, my_ensembles[0]] +
        allFlows[:, :, 1, my_ensembles[1]] +
        allFlows[:, :, 2, my_ensembles[2]],
        0
    )
)
