"""
purpose: convert/unpack a list of 2-tuples to two tuple lists
usage: x,y = convertLoT2NTs(thelistoftuples)
for turning it back, see: convertTup2LoT

:param myListOfTuples: a list of tuples

:return: depending on the size of the tuple, a number of lists are returned
If a list of 2-tuples are given, 2 lists are returned.
If a list of 3-tuples are given, 3 lists are returned.

example:
===========================================================================
"""

from supplychainmodelhelper import graphoperations as go
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']

myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)
receiverIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer'],
    products=['milk']
)

myEdges4Graph = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=senderIDs,
    incomingNodes=receiverIDs
)
list1, list2 = go.convertLoT2NTs(myEdges4Graph)
print(myEdges4Graph)
print(list1)
print(list2)
