"""
purpose: put in 2 lists, get out all possible combinations of element pairs
in 2 lists

:param list1: list with X elements
:param list2: list with Y elements
:param order: (optional) str for ordering which list should iterate fast
and which slow
default '1st': elements of list1 run fast and elements of list2 run slow
other '2nd': elements of list2 run fast and elements of list1 run slow

:return: 2 lists with all pair-wise combinations

example:



===============================================================================================
"""
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']

myNewList1,myNewList2 = go.getAllCombinations(prod,act)

print(myNewList1)
#>> ['milk', 'beer', 'schnaps', 'milk', 'beer', 'schnaps', 'milk', \\
#    'beer', 'schnaps', 'milk', 'beer', 'schnaps']
print(myNewList2)
#>> ['producer', 'producer', 'producer', 'consumer', 'consumer', \\
#    'consumer', 'warehouse', 'warehouse', 'warehouse',
#    'store', 'store', 'store']
