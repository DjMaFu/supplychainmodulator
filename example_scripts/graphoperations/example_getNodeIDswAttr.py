"""
TODO test, check if nodes exists?
purpose: get list of tuple with (nid,content of nodes with nid)
if no list of nodes are given, all nodes with this attribute are returned

:param SCGraph: the graph created with
createGraph(listOfActors: list, listOfLocations: list,
listOfProducts: list)
:param nameOfAttr: name of the existing attribute
:param listOfNodeIDs(optional): give a prepared list of node IDs

:return: list of tuples: (node id, attribute)




===========================================================================
"""

#example:
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

producerIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer']
)
print(go.getNodeIDswAttr(
        SCGraph=myNW,
        nameOfAttr='location',
        listOfNodeIDs=producerIDs
    )
)
