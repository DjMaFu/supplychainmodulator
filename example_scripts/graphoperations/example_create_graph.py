"""
purpose: create a directed graph with standard attributes for supply chain
modelling:
on creation of the graph a number of nodes are created based on the input
lists. The nodes contain all possible combinations of the elements from
the input list.
The attributes are: 'actor','location','product'

:param listOfActors: a list of participating actors (list of string)
:param listOfLocations: a list of locations involved
(e.g. Landkreise in Germany)
:param listOfProducts: a list of products being transported
(e.g. vegetables, eggs, ...)

:return: Graph with nodes containing all possible combinations of actors,
locations and products


===========================================================================
"""

# example:
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']

myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

print(myNW.nodes(data=True))