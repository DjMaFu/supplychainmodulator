"""
TODO: test
purpose: convert 2 lists or 2 tuples into 1 list of tuples
if first argument is list of tuples, the return will be a list of 3 tuples
usage;
x = convertTup2LoT(tuple or list ,tuple or list)                or
x = convertTup2LoT(list of tuples, tuple or list )
for turning back, see convertLoT2NTs

:param t1: tuple or list
:param t2: tuple or list

:return: a list of tuples


 ==========================================================================
"""
#example:
from supplychainmodelhelper import graphoperations as go
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']

myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)

someContent = [2, 4, 2]
contentConnected2NodeID = go.convertTup2LoT(senderIDs, someContent)
print(senderIDs)
print(someContent)
print(contentConnected2NodeID)

