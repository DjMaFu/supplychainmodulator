{'actor': 'producer', 'product': 'milk', 'location': 'BER'}
{'actor': 'producer', 'product': 'milk', 'location': 'SXF'}
{'actor': 'producer', 'product': 'milk', 'location': 'TXL'}
{'actor': 'producer', 'product': 'beer', 'location': 'BER'}
{'actor': 'producer', 'product': 'beer', 'location': 'SXF'}
{'actor': 'producer', 'product': 'beer', 'location': 'TXL'}
{'actor': 'producer', 'product': 'schnaps', 'location': 'BER'}
{'actor': 'producer', 'product': 'schnaps', 'location': 'SXF'}
{'actor': 'producer', 'product': 'schnaps', 'location': 'TXL'}
Is it stored into the graph: True
{'actor': 'producer', 'product': 'milk', 'location': 'BER', 'ags': '01111'}
{'actor': 'producer', 'product': 'milk', 'location': 'SXF', 'ags': '01112'}
{'actor': 'producer', 'product': 'milk', 'location': 'TXL', 'ags': '01113'}
{'actor': 'producer', 'product': 'beer', 'location': 'BER', 'ags': '01111'}
{'actor': 'producer', 'product': 'beer', 'location': 'SXF', 'ags': '01112'}
{'actor': 'producer', 'product': 'beer', 'location': 'TXL', 'ags': '01113'}
{'actor': 'producer', 'product': 'schnaps', 'location': 'BER', 'ags': '01111'}
{'actor': 'producer', 'product': 'schnaps', 'location': 'SXF', 'ags': '01112'}
{'actor': 'producer', 'product': 'schnaps', 'location': 'TXL', 'ags': '01113'}
