"""
purpose: return a list of attributes that exist in this graph

possible are nodes: 'nodes' or 'n'
possible are edges: 'edges' or 'e'

:param SCGraph: the graph in question
:param gtype: (optional) default is 'nodes'

:return: a list of attributes that the user may access
(after initialising: standard attributes are:
'actor','location','product')


===========================================================================
"""
#example:
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

print(go.getExistingAttrs(SCGraph=myNW, gtype='nodes'))
print(go.getExistingAttrs(SCGraph=myNW, gtype='edges'))
