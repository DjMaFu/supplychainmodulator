"""
purpose: This model maps raw data onto the graph via proxy data.
A proportional equation is solved:
networkData / inputNr = proxyData / proxyData.

example:
If you have the national production of eggs and want to map this
on your network model with a location resolution on a county level and
you have the data for how many chicken live in each county, you could come
up with a proxyModel saying the number of chicken is proportional to the
eggs produced in this county.
Then:
inputNr is the national production of eggs(float), proxyData is the number
of chicken in each county(list) and proxyNr is the sum of all chicken in
all counties. The returned list is then the modelled production of eggs
in each county.

Shorter version of this example:
inputNr = national production of eggs
proxyData = list of tuples of (NodeID, number of chicken in each location)
     this can also be just a list, but you get a warning if you do that
proxyNr = total number of chicken
inputNr/ModelData = proxyNr/proxyData ->
ModelData = proxyData/proxyNr*inputNr

:param inputNr: float. (usage see explanation above)
:param proxyData: list or List of tuples
(if you want to connect your node IDs with the respective attribute).
(usage see explanation above)
:param proxyNr: float. (usage see explanation above)


:return: list of modelled data or list of tuples of
(NodeID, modelled data), depending on proxyData.



===========================================================================
"""

#example:
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)
senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)

nationalProduction = 100.
proxynatProduction = 10.

print('creating list for input for proxymodel')
proxyregProduction = [2, 4, 2]

regionalProduction = go.proxyModel(
    inputNr=nationalProduction,
    proxyData=proxyregProduction,
    proxyNr=proxynatProduction
)
print(regionalProduction)

print('creating list of tuples for input for proxymodel')
proxyregProdWithNodeIDs = go.convertTup2LoT(senderIDs, proxyregProduction)

regionalProduction = go.proxyModel(
    inputNr=nationalProduction,
    proxyData=proxyregProdWithNodeIDs,
    proxyNr=proxynatProduction
)
print(regionalProduction)


