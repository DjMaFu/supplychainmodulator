"""
purpose: You provide an existing supply chain network and list of actors,
products and locations, this function returns a list of node ids with
these attributes.
If an attribute is missing, this function assumes the user wants all nodes
of the missing attribute.
If an empty list is given, this function assumes the user searches for
nodes that dont have this particular attribute. This should result in an
Exception if zero nodes are found with not one of the properties.

NOTE that the returned list of node ids is ordered as the original list of
attributes given at SC creation.


:param SCGraph: the graph created with
createGraph(listOfActors: list, listOfLocations: list,
listOfProducts: list)
:param actors: (optional) a list of actors
:param products: (optional) a list of products
:param locations: (optional) a list of locations

:return: a list of node IDs filtered to the specifications of the user



===========================================================================
"""
#example:
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

producerIDs = go.getListOfNodeIDs(SCGraph=myNW, actors=['producer'])

print(producerIDs)
for nid in producerIDs:
    print(myNW.nodes()[nid])

