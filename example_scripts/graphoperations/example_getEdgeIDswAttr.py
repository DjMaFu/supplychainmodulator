"""
purpose: get me the edge attributes content from the edges i provide,
but include the edge ID in the list returned. Good for transparency.

:param SCGraph: the graph in question.
:param attr(optional): An existing attribute of an edge in the graph.
default:'weight'. Learn about existing attributes in the graph via
the function "getExistingAttrs".
:param listOfEdgeIDs(optional): A list of edge IDs given. default:
all active edges (gotten from "getEdgeID").

:return: a list of tuples with
(outgoing node IDs, incoming node IDs, chosen attr)
may be converted to 3 lists with
nodeOut, nodeIn, weight = convertLoTToNTs(
    getEdgeIDswAttr(SCGraph, attr, listofEdgeIDs)
)

example:


===========================================================================
"""
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

producerIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['schnaps']
)
consumerOfSchnapsIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    products=['schnaps'],
    actors=['producer']
)
myEdgeIDs = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=producerIDs,
    incomingNodes=consumerOfSchnapsIDs
)

listOfShipping = [10, 1, 2000]
listOfDistances = [50.2, 10.3, 111.2]

didItWork1 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdgeIDs,
    listOfContent=listOfShipping
)
didItWork2 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdgeIDs,
    listOfContent=listOfDistances,
    attr='distance'
)

print(go.getEdgeIDswAttr(
    SCGraph=myNW,
    attr='weight')
)
print(go.getEdgeIDswAttr(
        SCGraph=myNW,
        attr='distance',
        listOfEdgeIDs=myEdgeIDs
    )
)

nodeOut, nodeIn, weight = go.convertLoT2NTs(
    go.getEdgeIDswAttr(
        SCGraph=myNW,
        attr='distance',
        listOfEdgeIDs=[myEdgeIDs[1]]
    )
)
edgeID = go.convertTup2LoT(nodeOut, nodeIn)
print(edgeID)

