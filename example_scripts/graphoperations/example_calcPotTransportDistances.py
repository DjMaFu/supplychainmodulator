"""
purpose: calculates nrOfValues possible transport distances, given a range
of possible values. The minimum value of this range is the optimum flow
(see optFlow for details).
The maximum value of this range is the proportional flow (see propFlow for
details).
Both values are calculated internally and depend on the flow between
participants of supply and demand regarding a specific product.
NOTE that the node attributes 'output' for the supplier nodes and 'input'
for the receiver nodes of goods, as well as the edge attributes 'distance'
and 'weight' between ALL the participating nodes MUST NOT be missing!

:param SCGraph: the graph with information about outgoing goods from
senders and incoming goods for receivers
:param listOfSenderIDs: list of node IDs in G (should have 'output'
attribute)
:param listOfReceiverIDs: list of node IDs in G (should have 'input'
attribute)
:param nrOfValues: (optional) int value. Please insert the number of
possible transport distances (default is 5).

:return: a list of possible transport distances. needed for hymanModel to
determine beta (details see hymanModel)

example:



===========================================================================
"""
from supplychainmodelhelper import graphoperations as go
import pandas as pd

# creating the graph
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

# creating list of node IDs of participants
senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)
receiverIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer'],
    products=['milk']
)

# Creating Distance matrix
myData = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
myDF = pd.DataFrame(myData, index=loc)

# creating edgeIDs for adding 'distance' to graph
allcombIDs1, allcombIDs2 = go.getAllCombinations(
    senderIDs,
    receiverIDs,
    order='1st'
)
myEdges4Graph = go.getEdgeID(myNW, allcombIDs1, allcombIDs2)
didItWork3 = go.addAttr2Edges(
    myNW,
    myEdges4Graph,
    myDF.values.flatten(),
    attr='distance'
)

# supply and demand
sendingMilk = [10, 50, 40]
receivingMilk = [30, 30, 40]
go.addAttr2ExistingNodes(myNW, senderIDs, 'output', sendingMilk)
go.addAttr2ExistingNodes(myNW, receiverIDs, 'input', receivingMilk)

myTDs = go.calcPotTransportDistances(
    SCGraph=myNW,
    listOfSenderIDs=senderIDs,
    listOfReceiverIDs=receiverIDs
)

print(myTDs)
