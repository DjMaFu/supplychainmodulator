"""
purpose: if an actor has a brand, but is the same type, this can be used
to give it the correct designation
(e.g. retailer: Warehouse_ALDI or Store_ALDI a.s.o.)
usually such an actor type has multiple brands

:param actor: an element of the actor list
:param brand: a list of brands for different actors: actor_suffix

:return: a combined list

===========================================================================
"""

from supplychainmodelhelper import graphoperations as go

act = ['producer', 'consumer', 'warehouse', 'store']

newActors = go.combineActorBrand(
    actor='warehouse',
    brand=['ALDI', 'REWE', 'LIDL']
)

print(act)

act.remove('warehouse')
act.extend(newActors)

print(act)