"""
purpose: add an attribute to a given list of edge IDs
NOTE that if an edge ID is not recognized, a node with the address given
will be created with no attributes!

:param SCGraph: This is the graph in question.
:param listOfEdgeIDs: a list of 2-tuples gotten from getEdgeID
:param listOfContent: a list of numbers(e.g. double or int) in the same
order as the list of edges
:param attr(optional): default='weight'. Another str may be given
(e.g. distance).

:return: Boolean. True if all was entered into the graph correctly.

example:

===========================================================================
"""
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

producerIDs = go.getListOfNodeIDs(
    myNW,
    actors=['producer']
)
consumerOfSchnapsIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    products=['schnaps'],
    actors=['producer']
)
myEdgeIDs = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=producerIDs,
    incomingNodes=consumerOfSchnapsIDs
    )

listOfShipping = [10, 1, 2000]
listOfDistances = [50.2, 10.3, 111.2]

didItWork1 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdgeIDs,
    listOfContent=listOfShipping
)
didItWork2 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdgeIDs,
    listOfContent=listOfDistances,
    attr='distance'
)

print('Is the weight stored into the graph: ' + str(didItWork1))
print(myNW.edges.data("weight"))

print('Is the distance stored into the graph: ' + str(didItWork2))
print(myNW.edges.data("distance"))

