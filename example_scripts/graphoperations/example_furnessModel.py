"""
purpose: calculate the flow between the senders and the receivers of goods
given the free parameter beta, the distances between all locations of
participants and the distance model.

Heart of the gravity model with exponential distance model.
flow_i,j = N_i,j * O_i * D_j * exp(-beta*distmatrix_i,j)

- N_i,j is normalization values, determined by furness automatically.
- O_i is the goods sent out by supplier i.
- D_j is the goods demanded by receiver j.
- beta is a free parameter determining the average amount of flow
(determined by hymanModel)
- distmatrix_i,j is the geographical distance between supplier i and
receiver j (got from getDistMatrix)


:param SCGraph: the graph with information about outgoing goods from
senders and incoming goods for receivers
:param listOfSenderIDs: list of node IDs in G (should have 'output'
attribute)
:param listOfReceiverIDs: list of node IDs in G (should have 'input'
attribute)
:param beta: free parameter (float) determining the average amount of flow
:param edgeattrDistName(optional): str name of the edge attribute for the
geographical distance between nodes listOfSenderIDs, order of rows should
be identical to listOfReceiverIDs. Default is 'distance'
:param distanceModel(optional): 'exp'(default) for exponential, 'lin' for
linear

:return: flow between supplier and receiver nodes based on given beta.

example:



===========================================================================
"""
from supplychainmodelhelper import graphoperations as go
import pandas as pd

# creating the graph
prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

# creating list of node IDs of participants
senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk']
)
receiverIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['consumer'],
    products=['milk']
)

# Creating Distance matrix
myData = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
myDF = pd.DataFrame(myData, index=loc)

# creating edgeIDs for adding 'distance' to graph
allcombIDs1, allcombIDs2 = go.getAllCombinations(
    list1=senderIDs,
    list2=receiverIDs,
    order='1st'
)
myEdges4Graph = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=allcombIDs1,
    incomingNodes=allcombIDs2
)
didItWork3 = go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdges4Graph,
    listOfContent=myDF.values.flatten(),
    attr='distance'
)

# supply and demand
sendingMilk = [10, 50, 40]
receivingMilk = [30, 30, 40]
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=senderIDs,
    nameOfAttr='output',
    listOfAttr=sendingMilk
)
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=receiverIDs,
    nameOfAttr='input',
    listOfAttr=receivingMilk
)

# determining beta
myBeta = 0.1
flow = go.furnessModel(
    SCGraph=myNW,
    listOfSenderIDs=senderIDs,
    listOfReceiverIDs=receiverIDs,
    beta=myBeta,
    edgeattrDistName='distance',
    distanceModel='exp'
)

print(flow)
print(flow.sum(axis=1))
print(flow.sum(axis=0))


