"""
purpose: add a custom attribute to a specified list of node IDs to your
supply chain nodes with the correct IDs

:param SCGraph: the graph created with
createGraph(listOfActors: list, listOfLocations: list,
listOfProducts: list)
:param listOfNodeIDs:  should be a list of the nodes you want to add your
attribute to (if set to 'None', this function assumes, you want to give
all existing nodes this attribute)
:param nameOfAttr: should be a recognizable string
(e.g. 'outgoing', 'incoming', ...)
:param listOfAttr:should be the list of values given to the chosen
attributes name (length of this list should be equal to listOfNodeIDs)

:return: Boolean: True if all nodeIDs with corresponding attribute values
are added to the graph


===========================================================================
"""

#example:
from supplychainmodelhelper import graphoperations as go

prod = ['milk', 'beer', 'schnaps']
act = ['producer', 'consumer', 'warehouse', 'store']
loc = ['BER', 'SXF', 'TXL']
myNW = go.createGraph(
    listOfActors=act,
    listOfLocations=loc,
    listOfProducts=prod
)

producerIDs = go.getListOfNodeIDs(myNW, actors=['producer'])
# before adding new attribute to graph nodes
for nid in producerIDs:
    print(myNW.nodes()[nid])



listOfAGS = [
    '01111',
    '01112',
    '01113',
    '01111',
    '01112',
    '01113',
    '01111',
    '01112',
    '01113'
]
didItWork = go.addAttr2ExistingNodes(
   SCGraph=myNW,
   listOfNodeIDs=producerIDs,
   nameOfAttr='ags',
   listOfAttr=listOfAGS
)
print('Is it stored into the graph: '+str(didItWork))
# after adding new attribute to graph nodes
for nid in producerIDs:
    print(myNW.nodes()[nid])


