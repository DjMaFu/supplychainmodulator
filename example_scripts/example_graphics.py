from supplychainmodelhelper import graphoperations as go
import pandas as pd
from supplychainmodelhelper.drawer import Drawer

# creating the graph
prod = ['milk', 'eggs']
act = ['producer', 'store', 'consumer']
loc = ['BER', 'SXF', 'TXL', 'P', 'test']
myNW = go.createGraph(listOfActors=act,
                      listOfLocations=loc,
                      listOfProducts=prod)

# Creating transport matrix
myWeights = {loc[0]: [10, 5, 5, 10, 12],
              loc[1]: [2, 10, 10, 8, 0],
              loc[2]: [10, 8, 10, 12, 2],
              loc[3]: [50, 18, 12, 20, 5],
             loc[4]: [50, 18, 12, 20, 1]}
myDF = pd.DataFrame(myWeights, index=loc)

# entering distance information to the right nodes on the graph
senderIDs = go.getListOfNodeIDs(
    SCGraph=myNW,
    actors=['producer'],
    products=['milk'])
receiverIDs = go.getListOfNodeIDs(
    myNW,
    actors=['consumer'],
    products=['milk'])

# supply and demand for milk and enrich the graph with this data
sendingProd = [1, 4, 9, 15, 2]
receivingProd = [36, 47, 68, 79, 100]
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=senderIDs,
    nameOfAttr='output',
    listOfAttr=sendingProd)
go.addAttr2ExistingNodes(
    SCGraph=myNW,
    listOfNodeIDs=receiverIDs,
    nameOfAttr='input',
    listOfAttr=receivingProd)


# get edge IDs for enriching graph with distance matrix
allcombIDs1, allcombIDs2 = go.getAllCombinations(
    senderIDs,
    receiverIDs,
    order='1st')
myEdges4Graph = go.getEdgeID(
    SCGraph=myNW,
    outgoingNodes=allcombIDs1,
    incomingNodes=allcombIDs2)

# actual adding edges to graph
go.addAttr2Edges(
    SCGraph=myNW,
    listOfEdgeIDs=myEdges4Graph,
    listOfContent=myDF.values.flatten(),
    attr='weight')


###########################################################################
d = Drawer(myNW)
d.draw_it(product=['milk'], actor_type_names=['producer', 'consumer'])