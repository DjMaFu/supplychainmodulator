"""

purpose: retrieve metadata from dataset in hdf5 db

input:
:param folder_name: name of an existing folder within hdf5 database
:param name_dataset: the name of the dataset given in list of md schema
(first element of this list)

:return: a dataframe(2 column table) of information about an existing
dataset with headers: MD Names, Content

example:

=======================================================================

"""
from supplychainmodelhelper import mds
import pandas as pd

myfname = './testDCnew.hdf5'
testDC = mds.Datacube(
    h5file_name=myfname,
    rights='new'
)

# add a folder to your database
my_list2FillOut = [
    'newfolder',
    '1',
    'This is a test',
    'some kind of description text to describe ' +
    'the reason for the existence of this folder'
]
testDC.addFolder2ExistingDB(
    list_entries=my_list2FillOut
)

# Creating dataset
loc = ['BER', 'SXF', 'TXL']
my_data = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
my_df = pd.DataFrame(my_data, index=loc)
# add dataset to an existing folder
my_list2FillOut = [
    'distancematrix',
    'distmatrix.csv',
    'csv',
    '1',
    'Distance matrix',
    str(len(list(my_df.index))),
    str(len(list(my_df.columns))),
    'utf-8'
]
testDC.addDataSet2ExistingFolder(
    folder_name='newfolder',
    list_data_entries=my_list2FillOut,
    dataset_df=my_df
)

# retrieve existing dataset back from database
# (created by addDataSet2ExistingFolder)
print(
    testDC.getMDFromDataSet(
        folder_name='newfolder',
        name_dataset='distancematrix'
    )
)


