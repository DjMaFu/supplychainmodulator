"""
purpose: initialise the data object

input:
:param h5file_name(optional): path to the hdf5 file. If not existing, a
new file will be created. default file_name is 'defaultDB.hdf5'
:param rights(optional): user rights to access this database.
'add'(default) user may read/write, if no file by this name exists, the
file will be created.
'new' will overwrite any existing databases (good for testing the
database, less good for working with it).
Also on initialisation of this data object the given hdf5 database
will be accessed and the md schema from the db overwrites the
default md schema

:return: none

example:


======================================================================


"""
from supplychainmodelhelper import mds
myNewDB = mds.Datacube('test.hdf5','new')

#if needed the file_name is attached to this data object
print(myNewDB.h5file_name)

