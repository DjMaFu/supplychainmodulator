"""
purpose:
removes an existing dataset in an existing folder from database.

input:
:param folder_name: the name of the folder in the db
:param dataset_name: the name of the dataset in the folder 'folder_name'

:return: none

example:



=======================================================================
"""
from supplychainmodelhelper import mds
import pandas as pd

loc = ['BER', 'SXF', 'TXL']

myfname = './testDCnew.hdf5'
testDC = mds.Datacube(
    h5file_name=myfname,
    rights='new'
)

# creating a folder
my_list2FillOut = [
    'newfolder',
    '1',
    'This is a test',
    'some kind of description text to describe the reason' +
    'for the existence of this folder'
]
testDC.addFolder2ExistingDB(
    list_entries=my_list2FillOut
)

# Creating dataset
my_data = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
my_df = pd.DataFrame(my_data, index=loc)

# check current md schema
print(testDC.list_template_md_dataset)
#>>['Dataset','file_name','Format','ID','Title','Rows','Columns',
#'Encoding']

# add dataset to an existing folder
my_list2FillOut = [
    'distancematrix',
    'distmatrix.csv',
    'csv',
    '1',
    'Distance matrix',
    str(len(list(my_df.index))),
    str(len(list(my_df.columns))),
    'utf-8'
]
testDC.addDataSet2ExistingFolder(
    folder_name='newfolder',
    list_data_entries=my_list2FillOut,
    dataset_df=my_df
)

print(
    testDC.getMDFromFolder(
        folder_name='newfolder'
    )
)

testDC.removeDatasetFromFolder(
    folder_name='newfolder',
    dataset_name='distancematrix'
)

print(
    testDC.getMDFromFolder(
        folder_name='newfolder'
    )
)