"""
purpose:
User may export current md schema with function
exportTemplateMDSchemaofDataset2CSV to a csv file, then
edit csv file and then import the content into the
Datacube.

Checks if current md schema is compatible with imported csv.
Checks if input parameter folder_name exists as folder in database.
This function imports all assiociated files named in respective
md schema and imports them automatically into the database.
Search for files either in directory where csv file with md data is
stored or in a subdirectory named 'folder_name'. Returns error if
neither option is good.

User may want to import self created csv:
If csv file is not created by exportTemplateMDSchemaofDB2CSV
the separator used should be a ';'.



input:
:param folder_name: name of existing folder in database
if data files are stored in subdirectory, it should have
the same name
:param file_name: file path to where the template is
stored to a csv file. file_names given in md schema should
match the file_names on disk.

:return: dataframe to check given input

example:



=======================================================================

"""
from supplychainmodelhelper import mds
import pandas as pd

myNewDB = mds.Datacube(
    h5file_name='test.hdf5',
    rights='new'
)

myNewDB.addFolder2ExistingDB(
    list_entries=[
        'myfolder',
        'an id',
        'some title',
        'any description'
    ]
)

# toy data
loc = ['BER','SXF','TXL']
my_data = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
my_df = pd.DataFrame(my_data, index=loc)

#toy meta data
my_list2FillOut = [
            'distancematrix',
            'distmatrix.csv',
            'csv',
            '1',
            'Distance matrix',
            str(len(list(my_df.index))),
            str(len(list(my_df.columns))),
            'utf-8'
]
myNewDB.addDataSet2ExistingFolder(
    folder_name='myfolder',
    list_data_entries=my_list2FillOut,
    dataset_df=my_df
)
print(
    myNewDB.getMDFromFolder(
        folder_name='myfolder'
    )
)

myNewDB.exportTemplateMDSchemaofDataset2CSV(
    filepath_folder_schema='file_name4.csv'
)


#...
# user sees this in 'file_name.csv'
#'Dataset';'file_name';'Format';'ID';'Title';'Rows';'Columns';'Encoding'
# ...
# user fills in information
# >>> FILE MANIPULATION BY HAND <<<
#importMDDataofDBfromCSV
#'Dataset';'file_name';'Format';'ID';'Title';'Rows';'Columns
#';'Encoding'
#'data1';'data1.csv';'csv';'1';'my first title';'4';'3';'utf-8'
#'data2';'data2.csv';'csv';'2';'my new title';'12';'31';'utf-8'
#'data3';'data3.csv';'csv';'3';'my other title';'123';'13';'utf-8'
# ...

myNewDB.importMDDataofDatasetFromCSV(
    folder_name='myfolder',
    csv_file_name='file_name3.csv'
)
print(
    myNewDB.getMDFromFolder(
        folder_name='myfolder'
    )
)
