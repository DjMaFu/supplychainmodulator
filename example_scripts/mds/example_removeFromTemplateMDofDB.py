"""
purpose:
If no folders are inserted yet to the database, the md schema
wil be updated with no further warning. This can be undone
by the function add2TemplateMDofDB.
If at least 1 folder in db exists, remove this category from all
existing folders in db. The user gets a warning if the flag 'sure'
is NOT set to true.

input:
:param attr2remove: can be a list or a str
:param sure: (optional) set True, if database exists already and
existing entries are to be removed

:return: none

example:


=======================================================================
"""
from supplychainmodelhelper import mds

# initialising the datacube operations toolkit
myfname = './testDCnew.hdf5'
testDC = mds.Datacube(myfname, 'new')

# basic md template on creation
print('\nThe basic md schema:')
print(testDC.listOfTemplateMDofDB)

# extend the metadata schema for database
testDC.add2TemplateMDofDB(
    [
        'db category 1',
        'db category 2',
        'db category 3'
    ]
)

# check out the CURRENT metadata schema of the database
print('\nThe extended md schema:')
print(testDC.listOfTemplateMDofDB)

# Please note, that the metadata schema of the Datacube class itself is
# changed, not the instance, so even if you open a new instance of a
# Datacube within the same code run, the new metadata schema is set
new_fname = './a_new_file.hdf5'
some_new_test = mds.Datacube(new_fname, 'new')
print('\neverything new, but still the old md schema:')
print(some_new_test.listOfTemplateMDofDB)



# this can only be removed by this method
testDC.removeFromTemplateMDofDB(
    [
        'db category 1',
        'db category 2'
    ]
)
# or a string
print('\nremoving some of the new md schema:')
print(testDC.listOfTemplateMDofDB)
testDC.removeFromTemplateMDofDB('db category 3')

# check out the CURRENT metadata schema of the database
print('\nThe basic md schema again:')
print(testDC.listOfTemplateMDofDB)


