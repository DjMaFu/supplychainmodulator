"""
purpose: adding a list of metadata categories to the folder meta data

input:
:param list_additional_categories: a list of new categories
:param sure(optional) only asked for if an hdf5 contains folder or
dataset and the needs to be updated afterwards. New categories in db
will have the note 'no entry yet'

:return: none

example:


=======================================================================

"""
from supplychainmodelhelper import mds

# initialising the datacube operations toolkit
myfname = './testDCnew.hdf5'
testDC = mds.Datacube(myfname, 'new')

# basic md template on creation
print(testDC.list_template_md_dataset)

# extend the metadata schema for database
testDC.add2TemplateMDofDataset(
    [
        'data category 1',
        'data category 2',
        'data category 3'
    ]
)

# check out the CURRENT metadata schema of the database
print(testDC.list_template_md_dataset)

# if dataset are added, the flag 'sure' needs to be set to 'True'
# otherwise change requests are ignored
testDC.addFolder2ExistingDB(
    [
        'thisnewFolder',
        '2',
        's',
        'Descripcvdxction'
    ]
)