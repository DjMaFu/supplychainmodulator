"""
purpose: removing either 1 element as string or multiple elements of
metadata categories from ALL datasets.
There are no multiple schemas for different datasets.
Might be added later, if pressure is high.

input:
:param attr2remove: a str or a list of existing categories
:param sure: (Boolean) only set to true, if you're sure
:return: none

example:


=======================================================================

"""
from supplychainmodelhelper import mds

# initialising the datacube operations toolkit
myfname = './testDCnew.hdf5'
testDC = mds.Datacube(myfname, 'new')

# basic md template on creation
print(testDC.list_template_md_dataset)

# extend the metadata schema for database
testDC.add2TemplateMDofDataset(
    [
        'folder category 1',
        'folder category 2',
        'folder category 3'
    ]
)

# check out the CURRENT metadata schema of the database
print(testDC.list_template_md_dataset)

# removing 1 item
testDC.removeElementTemplateDataSetMD('folder category 2')
print(testDC.list_template_md_dataset)

# removing more items
testDC.removeElementTemplateDataSetMD(
    [
        'folder category 1',
        'folder category 3'
    ]
)
print(testDC.list_template_md_dataset)
