"""
purpose:
store the current metadata schema of the database as a
template csv file, for later import
creates a template DataFrame csv file
containing minimum mandatory metadata information
which can be filled in csv file
and read in via importMDSchemaofDBfromCSV


input:
:param filepath_db_schema: file path to where the template is
stored to a csv file

:return: none

example:


=======================================================================

"""
from supplychainmodelhelper import mds
myNewDB = mds.Datacube(
    h5file_name='test.hdf5',
    rights='new'
)

myNewDB.exportTemplateMDSchemaofDB2CSV(
    filepath_db_schema='myBasicMDschemaofDB.csv'
)

myNewDB.add2TemplateMDofDB(
    [
        'a new category',
        'another new one',
        'not another one of these'
    ]
)

myNewDB.exportTemplateMDSchemaofDB2CSV(
    filepath_db_schema='expandedMDschemaofDB.csv'
)

