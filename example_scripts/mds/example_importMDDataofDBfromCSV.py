"""
purpose:
User may export current md schema with function
exportTemplateMDSchemaofDB2CSV to a csv file, then
edit csv file and then import the content into the
Datacube.
NOTE that database should be freshly initialised and NOT
contain any other data than an extended md schema up to this point.
This case has not been tested.

MD schema of csv and current md schema of db (via add2TemplateMDofDB)
need to be identical (returns error if doesnt fit).
I.e. the first row of the csv file should contain the
fields of the md schema.
All folders are to be created in the database. The name
and the ID need to be unique (is enforced!).

User may want to import self created csv:
If csv file is not created by exportTemplateMDSchemaofDB2CSV
the separator used should be a ';'.

input:
:param csv_file_name: file_name of the csv file, filled in by user

:return:

example:



=======================================================================

"""
from supplychainmodelhelper import mds
myNewDB = mds.Datacube(
    h5file_name='test.hdf5',
    rights='new'
)

myNewDB.exportTemplateMDSchemaofDB2CSV(
    filepath_db_schema='file_name1.csv'
)

#...
# user sees this in 'file_name.csv'
# 'Folder';'ID';'Title';'Description'
#...
# >>> FILE MANIPULATION BY HAND <<<
# user fills in the fields : csv mock up of 'file_name.csv'
#'Folder';'ID';'Title';'Description'
#'test1';'1';'my first Title';'my first description'
#'test2';'2';'my second Title';'my second description'
#'test3';'3';'my third Title';'my third description'
#'test4';'4';'my fourth Title';'my fourth description'

#...

myNewDB.importMDDataofDBfromCSV(
    csv_file_name='file_name2.csv'
)

# show me the new folders incl. md schema
print(myNewDB.getMDfrom_db())

