"""
purpose:
if md schema exists, add row to metadata table in folder
add dataset to hdf5, reference to name of Dataset
add metadata scheme of dataset to folder md schema
NOTE that only 2D dataframes are tested to be stored in this DB!
NOTE that category 'ID' needs to be unique in this Folder,
otherwise returns error!

input:
:param folder_name: name of an existing folder within hdf5 database
:param list_data_entries: list of metadata information of current
md schema
:param dataset_df: a pandas dataframe with information about axis

:return:

example:



=======================================================================

"""
from supplychainmodelhelper import mds
import pandas as pd

loc = ['BER','SXF','TXL']

myfname = './testDCnew.hdf5'
testDC = mds.Datacube(myfname,'new')

# creating a folder
my_list2FillOut = [
    'newfolder',
    '1',
    'This is a test',
    'some kind of description text to describe the reason ' +
    'for the existence of this folder'
]
testDC.addFolder2ExistingDB(list_entries=my_list2FillOut)

# Creating dataset
my_data={loc[0]:[1,2,50],loc[1]:[2,1,49],loc[2]:[50,49,1]}
my_df = pd.DataFrame(my_data,index=loc)

# check current md schema
print(testDC.list_template_md_dataset)
#>>['Dataset','file_name','Format','ID','Title','Rows','Columns',
#'Encoding']

# add dataset to an existing folder
my_list2FillOut = [
    'distancematrix',
    'distmatrix.csv',
    'csv',
    '1',
    'Distance matrix',
    str(len(list(my_df.index))),
    str(len(list(my_df.columns))),
    'utf-8'
]
testDC.addDataSet2ExistingFolder(
    folder_name='newfolder',
    list_data_entries=my_list2FillOut,
    dataset_df=my_df
)
print(
    testDC.getMDFromDataSet(
        folder_name='newfolder',
        name_dataset='distancematrix'
    )
)
