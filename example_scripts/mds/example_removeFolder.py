"""
purpose:
removes an existing folder from database.

input:
:param folder_name: the name of the folder in the db
(first entry in md schema)

:return: none

example:


=======================================================================
"""
from supplychainmodelhelper import mds

myfname = './testDCnew.hdf5'
testDC = mds.Datacube(myfname,'new')

# add a folder to your database
my_list2FillOut = [
    'newfolder',
    '1',
    'This is a test',
    'some kind of description text to describe the ' +
    'reason for the existence of this folder'
    ]
testDC.addFolder2ExistingDB(
    list_entries=my_list2FillOut
)

print(testDC.getMDfrom_db())

testDC.removeFolder(
    folder_name='newfolder'
)

print(testDC.getMDfrom_db())
