"""
purpose:
stores the current metadata schema into an csv file.
information about dataset may be added and later import in via
importMDSchemaofDatasetFromCSV.


input:
:param filepath_folder_schema: file path to where the template is
stored to a csv file

:return: none

example:




=======================================================================
"""
from supplychainmodelhelper import mds

myNewDB = mds.Datacube(
    h5file_name='test.hdf5',
    rights='new'
)

myNewDB.exportTemplateMDSchemaofDataset2CSV(
    filepath_folder_schema='myBasicMDschemaforAllDatasets.csv'
)

# adding more categories
myNewDB.add2TemplateMDofDataset(
    list_additional_categories=[
        'something old',
        'something new'
    ]
)

myNewDB.exportTemplateMDSchemaofDataset2CSV(
    filepath_folder_schema='myAdvancedMDschemaforAllDatasets.csv'
)