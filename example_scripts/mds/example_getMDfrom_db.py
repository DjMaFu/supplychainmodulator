"""
purpose:
retrieve metadata from db, information about folders in this database
and which information they contain returned as pandas dataframe

input: no parameters needed, as each instance deals with just one
database

:return: a dataframe (multi-column table) of information about an
existing dataset with headers based on current md schema
(basic schema headers: Description, Folder, ID, Title)

example:


======================================================================
"""
from supplychainmodelhelper import mds

myfname = './testDCnew.hdf5'
testDC = mds.Datacube(
    h5file_name=myfname,
    rights='new'
)

# creating a folder
my_list2FillOut = [
    'newfolder',
    '1',
    'This is a test',
    'some kind of description text to describe the reason ' +
    'for the existence of this folder'
]
testDC.addFolder2ExistingDB(
    list_entries=my_list2FillOut
)


 # retrieve md info about database
 # (created by addDataSet2ExistingFolder)
print(testDC.getMDfrom_db())

# creating a folder
my_list2FillOut = [
    'one more folder',
    '2',
    'This is another test',
    'any kind of description text to describe the reason ' +
    'for the existence of this folder, really'
]
testDC.addFolder2ExistingDB(
    list_entries=my_list2FillOut
)

print(testDC.getMDfrom_db())