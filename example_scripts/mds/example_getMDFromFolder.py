"""
purpose:
retrieve metadata from folder, information about the folder in question
and which information they contain

input:
:param folder_name: name of an existing folder within hdf5 database

:return:  a dataframe(2 column table) of information about an existing
dataset with headers: MD Names, Content wit list of all datasets

example:



=======================================================================

"""
from supplychainmodelhelper import mds

myfname = './testDCnew.hdf5'
testDC = mds.Datacube(
    h5file_name=myfname,
    rights='new'
)

md_folder = [
    'newfolder',
    '1',
    'This is a test',
    'some kind of description text to describe ' +
    'the reason for the existence of this folder'
]
testDC.addFolder2ExistingDB(
    list_entries=md_folder
)

# retrieve existing folder back from database
# nothing in here right now
# see
print(
    testDC.getMDFromFolder(
        folder_name='newfolder'
    )
)
