"""
purpose:
Adding a new category to the MD schema of the database is done by
the function add2TemplateMDofDB (see example).
Using this function only editing of EXISTING md categories is
supported.
Adding a new category means an update to md data of database, while
This function only edits (or adds) content to 1 entry of the database
at a time.

TODO:
Future updates may have the option of having the variable dataset as
a list and content as a list, so that multiple entries may be done
at the same time.

TODO:
Is a flag for overwriting existing content necessary?


input:
:param folder_name: the folder for which the content is for (str or list)
:param category: existing categories for changing (str) - 1 category
at a time
:param content: corresponding content existing category and
(str or list of) folders for changing (str or list) - same dimension
as folder

:return:

example:



=======================================================================

"""
from supplychainmodelhelper import mds
myNewDB = mds.Datacube('test.hdf5', 'new')

# add 3 folders
folder1 = [
    'test',
    '1',
    'a title',
    'a description'
]
folder2 = [
    'anothertest',
    '2',
    'another title',
    'a new description'
]
folder3 = [
    'yetanothertest',
    '3',
    'yet another title',
    'also new description'
]
myNewDB.addFolder2ExistingDB(
    list_entries=folder1
)
myNewDB.addFolder2ExistingDB(
    list_entries=folder2
)
myNewDB.addFolder2ExistingDB(
    list_entries=folder3
)

# add categories without being sure (gets an error, because user
# wasnt sure)
myNewDB.add2TemplateMDofDB(
    [
        'source',
        'year',
        'help'
    ]
)

# add categories with being sure
myNewDB.add2TemplateMDofDB(
    list_additional_categories=['source', 'year', 'help'],
    sure=True
)

# edit new entries
folder_names = [
    folder1[0],
    folder2[0],
    folder3[0]
]
myContent = [
    'Astronomische Nachrichten',
    'Zentralblatt Mathematik',
    'FMJ'
]
myNewDB.editMDofDB(
    folder_name=folder_names,
    category='source',
    content=myContent
)
# edit other entries
myContent = [
    '2009',
    '2021',
    '2017'
]
myNewDB.editMDofDB(
    folder_name=folder_names,
    category='year',
    content=myContent
)

#see changes
print(myNewDB.getMDfrom_db())

