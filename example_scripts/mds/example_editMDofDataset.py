"""
purpose:
adding a new category to md schema of dataset is done by
the function add2TemplateMDofDataset (see example).
Using this function only editing of EXISTING md categories is
supported.
Adding a new category means an update to all datasets, while
this function only edits (or adds) content to 1 dataset at a time.

This function checks if all inputs (folder, dataset, category) exists
before editing the field.

NOTE that the folder md data is updated as well, since the folder md
is just a summary of what is in the folder.

TODO:
Future updates may have the option of having the variable dataset as a
list and content as a list, so that multiple entries may be done at
the same time.

TODO:
Is a flag for overwriting existing content necessary?

input:
:param folder_name: existing folder in db (str) - assumed all datasets are
in given folder
:param name_dataset: existing dataset (str)
:param category: existing category (str) for changing 1 category
at a time
:param content: corresponding content existing category for changing

:return: none

example:


=======================================================================

"""
from supplychainmodelhelper import mds
import pandas as pd
myNewDB = mds.Datacube(
    h5file_name='test.hdf5',
    rights='new'
)

# adding folder_names to database
newfolder_nameMD = [
    'newfolder_name',
    '1',
    'A new folder_name',
    'A very cool description'
]
myNewDB.addFolder2ExistingDB(newfolder_nameMD)

# adding dataset to folder_name
loc = ['BER', 'TXL', 'SXF']
my_data = {
    loc[0]: [1, 2, 50],
    loc[1]: [2, 1, 49],
    loc[2]: [50, 49, 1]
}
my_df = pd.DataFrame(my_data, index=loc)
dataMD = [
    'distanceMatrix',
    'no file_name',
    'pandas df',
    '1',
    'distance matrix',
    '3',
    '3',
    'utf-8'
]
myNewDB.addDataSet2ExistingFolder(
    folder_name=newfolder_nameMD[0],
    list_data_entries=dataMD,
    dataset_df=my_df
)

# adding new md schema to dataset (user gets error, because at least
# 1 dataset is stored to database
myNewDB.add2TemplateMDofDataset(
    list_additional_categories=[
        'Creator',
        'time of creation'
    ]
)

# adding new md schema to dataset, being sure
myNewDB.add2TemplateMDofDataset(
    list_additional_categories=[
        'Creator',
        'time of creation'
    ],
    sure=True
)

# get md info about dataset
print(
    myNewDB.getMDFromDataSet(
        folder_name='newfolder_name',
        name_dataset='distanceMatrix'
    )
)

# editing dataset
myNewDB.editMDofDataset(
    folder_name='newfolder_name',
    name_dataset='distanceMatrix',
    category='Creator',
    content='Marcel'
)
myNewDB.editMDofDataset(
    folder_name='newfolder_name',
    name_dataset='distanceMatrix',
    category='time of creation',
    content='today'
)
