"""

if folder/dataset in db exists, add this new element to all existing
give user warning that missing entries need to be added to dataset by
other function
purpose: adding a list of metadata categories to the overall metadata
structure of the database

input:
:param list_additional_categories: a list of new categories
:param sure(optional) only asked for if database named in
initialisation, already contains folders.
New categories in db will have the note 'no entry yet'


:return: none

example:



=======================================================================
"""
from supplychainmodelhelper import mds

# initialising the datacube operations toolkit
myfname = './testDCnew.hdf5'
testDC = mds.Datacube(myfname,'new')

# basic md template on creation
print(testDC.listOfTemplateMDofDB)

# extend the metadata schema for database
testDC.add2TemplateMDofDB(['db category 1','db category 2',
'db category 3'])

# check out the CURRENT metadata schema of the database
print(testDC.listOfTemplateMDofDB)
