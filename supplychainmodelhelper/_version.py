# DONE for this increment
#   -
__version__ = "0.2.8"

# TODO:
#   - update documentation
#   - DATAHANDLING
#       - restructure datahandling (use mds and combine everything?)
#       - turn datahandling into class
#   - GRAPHOPERATIONS
#       - turn set of functions into class
#       - update help of proxyModel:
#           - input can be a tuple
#       - proxyModel: make it possible that input maybe a dict
#       - getnodeIDswAttr should also include search function with node
#       properties
#       - return should be a dict or a tuple
#       - some transport distances are numerically unstable:
#           - Solution 1: avoid transport distance when this happens (a bit
#           messy, but transport distances are a free parameter anyway.
#           apparently, if choosing a different but still close transport
#           distance, this doesnt happen). This could be done by a
#           brute-force algorithm (since hyman is very quick): catch
#           resonating solution, then try again with randomly chosen
#           different value within given interval.
#           - Solution 2: add algorithm to control minimization step size (aka.
#           Variable Step Solvers or Automatic Step-Size Control for
#          Minimization Iterations). Cleanest solution but also needs
#           development and testing time.
#           - Solution 3: add algorithm for scanning complete parameter space (
#          numerical effort highest, but also gives perfect solution for
#           calibration)
#     - create routine that returns the interval of transport distances (
#     min, max), so user may make up his/her own mind.
#   - MDS
#       - load from datacube
#           - turn into
#               - matrix
#              - pandas dataframe
#               - object with md attached
#               - dictionary
#       - bug fix: axis not in axis - something about the loading function
#      - metadata list: turn into dictionary and trigger warning if not added
#       correctly
#      - import textfields of table as axis
#      - fix import function (md schema of data set multiplied)
#      - addFolder2ExistingDB input should be a dict or an object
#      - addFolder2ExistingDB ID should be given automatically
#      - addDataSet2ExistingFolder ID should be given automatically
#      - add excel reader to import function
#   - DRAWER:
#       -   see example_graphic: edge heatmap in not consistent with node input
#   - GUI:
#       - tkinter
